package org.wit.myrent.app;

/**
 * Created by ictskills on 30/09/15.
 */
import org.wit.myrent.models.Portfolio;
import android.app.Application;
import static org.wit.android.helpers.LogHelpers.info;
import org.wit.myrent.models.PortfolioSerializer;

public class MyRentApp extends Application
{
  private static final String FILENAME = "portfolio.json";
  public Portfolio portfolio;

  @Override
  public void onCreate()
  {
    super.onCreate();
    PortfolioSerializer serializer = new PortfolioSerializer(this, FILENAME);
    portfolio = new Portfolio(serializer);

    info(this, "RentControl app launched");
  }
}
