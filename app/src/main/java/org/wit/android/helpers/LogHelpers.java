package org.wit.android.helpers;

/**
 * Created by ictskills on 30/09/15.
 */

import android.util.Log;

public class LogHelpers
{
  public static void info(Object parent, String message)
  {
    Log.i(parent.getClass().getSimpleName(), message);
  }
}
